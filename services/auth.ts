import { fetchData } from "@/utils"

export const authentication = async (body: any) => {
  return await fetchData('/login', 'POST', body)
}