const baseUrl = process.env.NEXT_PUBLIC_BASE_URL

export const fetchData = async (url: string, method:string, body?:any, token?: any) => {
  try {
    const response = await fetch(baseUrl + url, {
      method,
      headers: getHeaders(token),
      body: JSON.stringify(body),
    });
    if (!response.ok) {
      throw new Error("Network response was not ok");
    }
    const data = await response.json();
    return data;
  } catch (error) {
    console.error("An error occurred while fetching data: ", error);
  }
};

const getHeaders = (authToken?: any) => {
  const headers: any = {
    'Content-Type': 'application/json',
  };
  if (authToken) {
    headers.Authorization = `Bearer ${authToken}`;
  }
  return headers;
};
