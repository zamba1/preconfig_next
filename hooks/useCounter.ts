import { useContext } from 'react'
import { MyContext } from '@/store/MyContext'
import { authentication } from '@/services'

export const useCounter = () => {
  const { state: { count }, dispatch } = useContext(MyContext)

  const loginAndIncrement = async (body: any) => {
    const data = await authentication(body)
    console.log({data})
    dispatch({ type: 'INCREMENT' })
  }

  return {
    count,
    loginAndIncrement
  }
}