import { useContext } from 'react'
import { MyContext } from '@/store/MyContext'

export const useToggleMode = () => {
  const { state: { theme }, dispatch } = useContext(MyContext)

  const toggleMode = () => {
    console.log('loogog')
    dispatch({ type: 'TOGGLE_MODE' })
  }

  return {
    theme,
    toggleMode
  }
}