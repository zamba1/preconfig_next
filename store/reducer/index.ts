import { stateType, actionType } from "../type"

export const reducer = (state: stateType, action: actionType) => {
  switch (action.type) {
    case 'INCREMENT':
      return {
        ...state,
        count: state.count as number + 1
      }
    case 'DECREMENT':
      return {
        ...state,
        count: state.count as number - 1
      }
    case 'TOGGLE_MODE':
        console.log("dispatch")
        return {
          ...state,
          theme: state.theme === 'light' ? 'dark' : 'light'
        }
    default:
      return state
  }
}