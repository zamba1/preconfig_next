import { stateType } from "../type"

export const initialstate: stateType = {
    count: 0,
    theme: 'light'
}