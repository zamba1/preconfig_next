
export interface stateType {
    count: number,
    theme: string
}

export interface actionType {
    type: String,
    payload?: Object
}