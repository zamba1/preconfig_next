import React, { createContext, useReducer } from 'react'; 
import { reducer } from './reducer';
import { initialstate } from './state';
import { MyContext } from './MyContext';

interface Props {
    children: React.ReactNode;
}

export const MyProvider: React.FC<Props> = ({ children }) => { 
  const [state, dispatch] = useReducer(reducer, initialstate);

  return (
    <MyContext.Provider value={{ state, dispatch }}>
      {children}
    </MyContext.Provider>
  );
};