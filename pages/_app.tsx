import { MyProvider } from '@/store'
import { AppProps } from 'next/app'
import { ThemeProvider } from '@mui/material/styles'
import CssBaseline from '@mui/material/CssBaseline'
import { CacheProvider, EmotionCache } from '@emotion/react'
import lightTheme from '@/styles/theme/lightTheme'
import darkTheme from '@/styles/theme/darkTheme'
import createEmotionCache from '@/utils/createEmotionCache'
import { useToggleMode } from '@/hooks'

const clientSideEmotionCache = createEmotionCache()

export interface MyAppProps extends AppProps {
  emotionCache?: EmotionCache;
}

export default function MyApp({ Component, pageProps, emotionCache = clientSideEmotionCache }: MyAppProps) {
  
  return (  
    <MyProvider>
      <MyAppContainer Component={Component} pageProps={pageProps} emotionCache={emotionCache} />
    </MyProvider>
  )
}

function MyAppContainer({ Component, pageProps, emotionCache }: any) {
  const { theme } = useToggleMode()
  return (
    <CacheProvider value={emotionCache}>
      <ThemeProvider theme={theme === 'dark' ? darkTheme : lightTheme}>
        <CssBaseline />
        <Component {...pageProps} />
      </ThemeProvider>
    </CacheProvider>
  );
}