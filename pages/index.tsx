
import { Inter } from 'next/font/google'
import { useCounter } from '@/hooks'
import Button from '@mui/material/Button'
import { useToggleMode } from '@/hooks'
import { useStylesWelcome } from '@/styles/stylesMUI'
import Typography from '@mui/material/Typography';
import Chip from '@mui/material/Chip';
const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  const { toggleMode } = useToggleMode()
  const cls = useStylesWelcome()

  const changeTheme = async () => {
    toggleMode()
  }
  return (
    <div className={cls.root}>
      <div className={cls.cardWelcome}>
        <div>
          <Typography variant="h4" gutterBottom>
            Preconf Next 13 Project
          </Typography>
          <Typography variant="body1" gutterBottom>
            <a className={cls.link} target='_blank' href="https://gitlab.com/zamba1/preconfig_next">With this pre-config project</a>, you can directly use all packages and create directly pages and use your own system design like Atomic design or standard component system,
            the features that you can use after running<span className={cls.chip}> npm install --force </span> are :
          </Typography>
          <Chip sx={{ mr: 1, mt: 1 }} label="Light and dark theme pre-config" />
          <Chip sx={{ mr: 1, mt: 1 }} label="Global state using Context and reducer" />
          <Chip sx={{ mr: 1, mt: 1 }} label="API service client using fetch api" />
          <Chip sx={{ mr: 1, mt: 1 }} label="MUI, Material Style, Material system" />
        </div>
        <Button sx={{ mt: 4 }} onClick={changeTheme} >Change theme</Button>
      </div>
    </div>
  )
}
