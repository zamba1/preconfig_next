import { Theme } from '@mui/material/styles'
import { makeStyles, createStyles  } from '@mui/styles'

export const useStylesWelcome: any = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
      position: 'relative',
      height: '100vh'
    },
    cardWelcome: {
      width: '50%',
      height: 'auto',
      borderRadius: 10,
      boxShadow: theme.palette.mode === 'light' ? '1px 6px 6px -1px rgba(122,111,122,0.3);' : 'none',
      padding: '20px',
      border: theme.palette.mode === 'dark' ? '2px solid #2196F3;' : 'none',
    },
    chip: {
      borderRadius: 50,
      padding: '1px 10px',
      margin: '0px 5px',
      border: theme.palette.mode === 'dark' ? '1px solid #2196F3;' : '1px solid #FE6B8B;',
    },
    link: {
      cursor: 'pointer',
      textDecoration: 'none',
      position: 'relative',
      transition: 'all .3s ease-out',
      color: theme.palette.mode === 'dark' ? 'white' : 'black',
      '&:hover': {
        color: '#fff',
      },
      '&:after': {
        content: '""',
        background: theme.palette.mode === 'light' ? 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)' : 'linear-gradient(45deg, #2196F3 30%, #21CBF3 90%)',
        position: 'absolute',
        width: '100%',
        height: '2px',
        bottom: 0,
        rigth:0,
        left: 0,
        transition: 'all .3s ease-out',
        zIndex: -1
      },
      '&:hover:after': {
        width: '100%',
        height: '20px',
      },
    }
  })
);