import { ThemeOptions, createTheme } from '@mui/material/styles';

const darkTheme: ThemeOptions = createTheme({
  palette: {
    mode: 'dark',
  },
  components: {
    // Name of the component
    MuiButton: {
      styleOverrides: {
        // Name of the slot
        root: {
          // Some CSS
          background: 'linear-gradient(45deg, #2196F3 30%, #21CBF3 90%)',
          border: 0,
          borderRadius: 3,
          boxShadow: '0 3px 5px 2px rgba(33, 203, 243, .3)',
          color: 'white',
          padding: '10px 30px',
        },
      },
    },
  },
})

export default darkTheme